package fr.kolb.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.kolb.bean.Client;
import fr.kolb.bean.Commande;





public class CommandeDaoImpl implements CommandeDao {

	@Override
	public void addCommande(Commande commande) {
		
		try {
			
			String query ="insert into commandes values (?, ?, ?, ?)";
			
			PreparedStatement pstat = DAOFactory.getConnection().prepareStatement(query);
			
			pstat.setInt(1, commande.getNumCommande());
			pstat.setInt(2, commande.getNumClient());
			pstat.setDate(3, new java.sql.Date(commande.getDateCommande().getTime()));
			pstat.setDouble(4, commande.getMontantCommande());
			
			int executeUpdate = pstat.executeUpdate();
			System.out.println(executeUpdate + " row(s) affected");
			System.out.printf("%d row(s) affected", executeUpdate); //v2.0
			
			pstat.close();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void updateCommande(Commande commande) {
		
		try {
			String query = "update commandes set n_cli = ?, date_com = ?, montant_com = ? where n_com = ?";
			
			PreparedStatement pstat = DAOFactory.getConnection().prepareStatement(query);
			pstat.setInt(1, commande.getNumClient());
			pstat.setDate(2, new java.sql.Date(commande.getDateCommande().getTime()));
			pstat.setDouble(3, commande.getMontantCommande());
			
			pstat.setInt(4, commande.getNumCommande());    

			int executeUpdate = pstat.executeUpdate();
			System.out.println(executeUpdate + " row(s) affected");
			System.out.printf("%d row(s) affected", executeUpdate); //v2.0
			
			pstat.close();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void deleteCommande(Commande commande) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Commande> getAllCommande() {
		ArrayList<Commande> commandes = new ArrayList<Commande>();

		try {
			Statement stat = DAOFactory.getConnection().createStatement();
			ResultSet result = stat.executeQuery("select * from commandes");


			while (result.next()) {

				Commande commande = new Commande(result.getInt("n_com"), result.getInt("n_cli"),
						result.getDate("date_com"), result.getDouble("montant_com"));
				commandes.add(commande);
			}

			result.close();
			stat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return commandes;
	}

	@Override
	public Commande getCommandeByCommandeId(int id) {
		Commande commande = new Commande();

		String sql = "select * from commandes where n_com = ?";
		try {
			PreparedStatement pstat = DAOFactory.getConnection().prepareStatement(sql);
			pstat.setInt(1, id);
			ResultSet result = pstat.executeQuery();

			while (result.next()) {
				commande.setNumCommande(result.getInt("n_com"));
				commande.setNumClient(result.getInt("n_cli"));
				commande.setDateCommande(result.getDate("date_com"));
				commande.setMontantCommande(result.getDouble("montant_com"));
			}

			result.close();
			pstat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return commande;
	}

	@Override
	public List<Commande> getAllCommandeByClient(Client client) {
		ArrayList<Commande> commandes = new ArrayList<Commande>();

		try {
			String sql = "select * from commandes where n_cli = ?";
			PreparedStatement pstat = DAOFactory.getConnection().prepareStatement(sql);
			pstat.setInt(1, client.getNumCLient());
			ResultSet result = pstat.executeQuery();

			while (result.next()) {

				Commande commande = new Commande(result.getInt("n_com"), result.getInt("n_cli"),
						result.getDate("date_com"), result.getDouble("montant_com"));
				commandes.add(commande);
			}

			result.close();
			pstat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return commandes;
	}

	@Override
	public List<Commande> getAllCommandeByClientId(int id) {
		ArrayList<Commande> commandes = new ArrayList<Commande>();

		try {
			String sql = "select * from commandes where n_cli = ?";
			PreparedStatement pstat = DAOFactory.getConnection().prepareStatement(sql);
			pstat.setInt(1, id);
			ResultSet result = pstat.executeQuery();

			while (result.next()) {

				Commande commande = new Commande(result.getInt("n_com"), result.getInt("n_cli"),
						result.getDate("date_com"), result.getDouble("montant_com"));
				commandes.add(commande);
			}

			result.close();
			pstat.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return commandes;
	}

}
