package fr.kolb.dao;

import java.util.List;

import fr.kolb.bean.Commande;
import fr.kolb.bean.Client;

public interface CommandeDao {

	public void addCommande(Commande commande);
	public void updateCommande(Commande commande);
	public void deleteCommande(Commande commande);
	public List<Commande> getAllCommande();
	public Commande getCommandeByCommandeId(int id); 
	public List<Commande> getAllCommandeByClient(Client client);
	public List<Commande> getAllCommandeByClientId(int id);
}
