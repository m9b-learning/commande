package fr.kolb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOFactory {

	private static Connection connexion;

	private final static String URL = "jdbc:oracle:thin:@localhost:10021:xe";
	private final static String LOGIN = "basetest";
	private final static String PASSWORD = "root";

	public static DAOFactory getInstance() {
		return new DAOFactory();
	}

	public static Connection getConnection() {

		if (connexion == null) {
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				connexion = DriverManager.getConnection(URL, LOGIN, PASSWORD);
				connexion.setAutoCommit(false);
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return connexion;

	}

	public static void closeConnection() {
		try {
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public CommandeDao getCommandeDao()
	{
		return new CommandeDaoImpl();
	}

}
