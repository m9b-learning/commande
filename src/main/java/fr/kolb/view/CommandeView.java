package fr.kolb.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.kolb.dao.CommandeDao;
import fr.kolb.dao.CommandeDaoImpl;

/**
 * Servlet implementation class Commande
 */
@WebServlet("/Commande")
public class CommandeView extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommandeView() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CommandeDao cmdDAO = new CommandeDaoImpl();
		if (request.getParameter("client") != null) {
			String id = request.getParameter("client");
			request.setAttribute("commandes", cmdDAO.getAllCommandeByClientId(Integer.parseInt(id)));
			
		} else {
			request.setAttribute("commandes", cmdDAO.getAllCommande());
			
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/commande.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
