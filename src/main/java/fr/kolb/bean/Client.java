package fr.kolb.bean;

import java.io.Serializable;

public class Client implements Serializable {
	
	private static final long serialVersionUID = 2L;
	private int numCLient;
	private String nomClient;
	private String prenomClient;
	private String adresse1;
	private String adresse2;
	private String cpClient;
	private String villeClient;
	private String emailClient;

	public Client() {
	}

	public Client(int numCLient, String nomClient, String prenomClient, String adresse1, String adresse2,
			String cpClient, String villeClient, String emailClient) {
		this.numCLient = numCLient;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresse1 = adresse1;
		this.adresse2 = adresse2;
		this.cpClient = cpClient;
		this.villeClient = villeClient;
		this.emailClient = emailClient;
	}

	public int getNumCLient() {
		return numCLient;
	}

	public void setNumCLient(int numCLient) {
		this.numCLient = numCLient;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public String getAdresse1() {
		return adresse1;
	}

	public void setAdresse1(String adresse1) {
		this.adresse1 = adresse1;
	}

	public String getAdresse2() {
		return adresse2;
	}

	public void setAdresse2(String adresse2) {
		this.adresse2 = adresse2;
	}

	public String getCpClient() {
		return cpClient;
	}

	public void setCpClient(String cpClient) {
		this.cpClient = cpClient;
	}

	public String getVilleClient() {
		return villeClient;
	}

	public void setVilleClient(String villeClient) {
		this.villeClient = villeClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adresse1 == null) ? 0 : adresse1.hashCode());
		result = prime * result + ((adresse2 == null) ? 0 : adresse2.hashCode());
		result = prime * result + ((cpClient == null) ? 0 : cpClient.hashCode());
		result = prime * result + ((emailClient == null) ? 0 : emailClient.hashCode());
		result = prime * result + ((nomClient == null) ? 0 : nomClient.hashCode());
		result = prime * result + numCLient;
		result = prime * result + ((prenomClient == null) ? 0 : prenomClient.hashCode());
		result = prime * result + ((villeClient == null) ? 0 : villeClient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (adresse1 == null) {
			if (other.adresse1 != null)
				return false;
		} else if (!adresse1.equals(other.adresse1))
			return false;
		if (adresse2 == null) {
			if (other.adresse2 != null)
				return false;
		} else if (!adresse2.equals(other.adresse2))
			return false;
		if (cpClient == null) {
			if (other.cpClient != null)
				return false;
		} else if (!cpClient.equals(other.cpClient))
			return false;
		if (emailClient == null) {
			if (other.emailClient != null)
				return false;
		} else if (!emailClient.equals(other.emailClient))
			return false;
		if (nomClient == null) {
			if (other.nomClient != null)
				return false;
		} else if (!nomClient.equals(other.nomClient))
			return false;
		if (numCLient != other.numCLient)
			return false;
		if (prenomClient == null) {
			if (other.prenomClient != null)
				return false;
		} else if (!prenomClient.equals(other.prenomClient))
			return false;
		if (villeClient == null) {
			if (other.villeClient != null)
				return false;
		} else if (!villeClient.equals(other.villeClient))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Client [numCLient=").append(numCLient).append(", nomClient=").append(nomClient)
				.append(", prenomClient=").append(prenomClient).append(", adresse1=").append(adresse1)
				.append(", adresse2=").append(adresse2).append(", cpClient=").append(cpClient).append(", villeClient=")
				.append(villeClient).append(", emailClient=").append(emailClient).append("]");
		return builder.toString();
	}

}
