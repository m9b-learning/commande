package fr.kolb.bean;

import java.io.Serializable;
import java.util.Date;

public class Commande implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int numCommande;
	private int numClient;
	private Date dateCommande;
	private double montantCommande;

	public Commande() {
	}

	public Commande(int numCommande, int numClient, Date dateCommande, double montantCommande) {
		this.numCommande = numCommande;
		this.numClient = numClient;
		this.dateCommande = dateCommande;
		this.montantCommande = montantCommande;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Commande [numCommande=").append(numCommande).append(", numClient=").append(numClient)
				.append(", dateCommande=").append(dateCommande).append(", montantCommande=").append(montantCommande)
				.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateCommande == null) ? 0 : dateCommande.hashCode());
		long temp;
		temp = Double.doubleToLongBits(montantCommande);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + numClient;
		result = prime * result + numCommande;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (dateCommande == null) {
			if (other.dateCommande != null)
				return false;
		} else if (!dateCommande.equals(other.dateCommande))
			return false;
		if (Double.doubleToLongBits(montantCommande) != Double.doubleToLongBits(other.montantCommande))
			return false;
		if (numClient != other.numClient)
			return false;
		if (numCommande != other.numCommande)
			return false;
		return true;
	}

	public int getNumCommande() {
		return numCommande;
	}

	public void setNumCommande(int numCommande) {
		this.numCommande = numCommande;
	}

	public int getNumClient() {
		return numClient;
	}

	public void setNumClient(int numClient) {
		this.numClient = numClient;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public double getMontantCommande() {
		return montantCommande;
	}

	public void setMontantCommande(double montantCommande) {
		this.montantCommande = montantCommande;
	}

}
