<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>commande</title>
<script src="js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/knacss.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

	<ul class="nav">
		<li><a href="Commande">Commande</a></li>
		<li><a href="CreateCommande">Nouvelle Commande</a></li>
	</ul>

	<h1>Nouvelle Commande</h1>
	<div class="txtleft mrl mll">

		<form action="CreateCommande" method="post">

			<ul class="is-unstyled">
				<li class="mbs">
					<label for="numClient" class="txtleft w15">N� Client <span class="rouge">*</span></label>
					<input type="number" id="numClient" name="numClient" value="" />
				</li>
				<li class="mbs">
					<label for="date" class="txtleft w15">Date <span class="rouge">*</span></label>
					<input type="date" id="date" name="dateCommande" value="" />
				</li>
				<li class="mbs">
					<label for="montant" class="txtleft w15">Date <span class="rouge">*</span></label>
					<input type="text" id="montant" name="montantCommande" value="" />
				</li>
				<li class="mbs">
					<button class="btn--primary" type="submit">Envoyer</button>
				</li>
			</ul>
		</form>
	</div>

	<script src="js/javascript.js"></script>
</body>
</html>