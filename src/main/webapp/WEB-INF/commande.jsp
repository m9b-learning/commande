<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>commande</title>
<script src="js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/knacss.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

	<ul class="nav">
		<li><a href="Commande">Commande</a></li>
		<li><a href="CreateCommande">Nouvelle Commande</a></li>
	</ul>
	
	<h1>Liste des Commandes</h1>
	<div class="txtcenter mrl mll">
		<table class="table--zebra">
			<thead>
				<tr>
					<th scope="col">N� Commande</th>
					<th scope="col">N� Client</th>
					<th scope="col">Date</th>
					<th scope="col">Montant</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${commandes}" var="item">
					<tr>
						<td><c:out value="${ item.numCommande }" /></td>
						<td><a href="?client=${ item.numClient }"><c:out value="${ item.numClient }" /></a></td>
						<td><c:out value="${ item.dateCommande }" /></td>
						<td><c:out value="${ item.montantCommande }" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<script src="js/javascript.js"></script>
</body>
</html>